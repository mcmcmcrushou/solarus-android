package org.solarus_games.solarus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.PixelCopy;
import android.view.View;
import android.widget.Toast;

import org.libsdl.app.SDLActivity;

public class SolarusEngine extends SDLActivity {
    public Quest quest;
    final private String TAG = "SolarusEngine";
    public Bitmap screenCapture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        quest = Quest.fromPath(getIntent().getExtras().getString("quest_path"));
        SolarusApp.setCurrentQuest(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    private void bringMainActivityToFront() {
        Intent i = new Intent(getApplicationContext(), Solarus.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        // Ignore certain special keys so they're handled by Android
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            View sv = mSurface;
            int width = sv.getWidth();
            int height = sv.getHeight();
            final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            PixelCopy.request(mSurface, bitmap, new PixelCopy.OnPixelCopyFinishedListener() {
                @Override
                public void onPixelCopyFinished(int i) {
                    screenCapture = bitmap;
                    bringMainActivityToFront();
                }
            }, new Handler(getMainLooper()));
            return true; //Let the back button go up
        }
        return super.dispatchKeyEvent(event);
    }

    public void exit() {
        SDLActivity.mExitCalledFromJava = true;
        SDLActivity.nativeQuit();

        // Now wait for the SDL thread to quit
        if (SDLActivity.mSDLThread != null) {
            try {
                SDLActivity.mSDLThread.join();
            } catch(Exception e) {
                Log.v(TAG, "Problem stopping thread: " + e);
            }
            SDLActivity.mSDLThread = null;
        }

        SolarusApp.engineDown(this);
        finish();
    }

    @Override
    protected String[] getArguments() {
        String[] args_emulator ={"-no-audio",quest.path};
        String[] args = {quest.path};
        return isEmulator() ? args_emulator : args;
    }
}
